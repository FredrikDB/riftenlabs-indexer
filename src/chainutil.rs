use std::collections::VecDeque;

// Function to calculate MTP from the last N timestamps
pub fn calculate_mtp(timestamps: &VecDeque<u32>) -> u32 {
    let mut sorted_timestamps = timestamps.clone();
    sorted_timestamps.make_contiguous().sort();
    sorted_timestamps[sorted_timestamps.len() / 2]
}
