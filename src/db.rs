use anyhow::Result;
use bitcoin_hashes::hex::ToHex;
use riftenlabs_defi::cauldron::ParsedContract;
use rusqlite::{params, Connection};

pub fn prepare_tables(conn: &Connection) {
    conn.execute(
        "CREATE TABLE utxo_funding (
        new_utxo_hash TEXT PRIMARY KEY,
        spent_utxo_hash TEXT,
        timestamp BIGINT,
        new_utxo_txid TEXT,
        new_utxo_n INT,
        sats BIGINT,
        token_amount BIGINT,
        token_id TEXT
    );",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE INDEX idx_funding_timestamp ON utxo_funding (timestamp);",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE INDEX idx_utxo_funding_token_id ON utxo_funding (token_id);",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE TABLE utxo_spending (
        spent_utxo_hash TEXT PRIMARY KEY,
        timestamp BIGINT
    );",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE INDEX idx_spending_timestamp ON utxo_spending (timestamp);",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE TABLE cfg_number (
        key TEXT PRIMARY KEY,
        value INT
    )",
        [],
    )
    .unwrap();

    // Indexes for list_by_volume
    conn.execute(
        "CREATE INDEX idx_utxo_funding_join ON utxo_funding(new_utxo_hash, sats, token_id);",
        [],
    )
    .unwrap();
    conn.execute("CREATE INDEX idx_utxo_funding_tvl_highest ON utxo_funding(token_id, new_utxo_hash, sats, token_amount);", []).unwrap();
}

pub fn config_set(conn: &Connection, key: &str, value: i64) {
    let mut stmt = conn
        .prepare("INSERT OR REPLACE INTO cfg_number (key, value) VALUES (?, ?)")
        .unwrap();
    stmt.execute(params![key, value]).unwrap();
}

pub fn config_get(conn: &Connection, key: &str) -> Result<Option<i64>> {
    let mut stmt = conn.prepare("SELECT value FROM cfg_number WHERE key = ?")?;

    let mut row = stmt.query([key])?;
    Ok(row.next()?.map(|r| r.get(0).unwrap()))
}

pub fn insert_utxo_funding(
    con: &Connection,
    timestamp: u32,
    cauldrons: &Vec<ParsedContract>,
) -> Result<()> {
    let mut statement = con
        .prepare("INSERT OR IGNORE INTO utxo_funding (new_utxo_hash, spent_utxo_hash, timestamp, new_utxo_txid, new_utxo_n, sats, token_amount, token_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")?;

    for c in cauldrons {
        if c.new_utxo_hash.is_none() {
            continue;
        }
        statement.execute(params![
            c.new_utxo_hash.unwrap().to_hex(),
            c.spent_utxo_hash.to_hex(),
            timestamp,
            c.new_utxo_txid.unwrap().to_hex(),
            c.new_utxo_n.unwrap(),
            c.sats.unwrap(),
            c.token_amount.unwrap(),
            c.token_id.unwrap().to_hex(),
        ])?;
    }
    Ok(())
}

pub fn insert_utxo_spending(
    conn: &Connection,
    timestamp: u32,
    cauldrons: &Vec<ParsedContract>,
) -> Result<()> {
    let mut statement = conn.prepare(
        "INSERT OR IGNORE INTO utxo_spending (spent_utxo_hash, timestamp) VALUES (?, ?)",
    )?;

    for c in cauldrons {
        statement.execute(params![c.spent_utxo_hash.to_hex(), timestamp])?;
    }
    Ok(())
}

pub fn get_token_tvl(
    connection: &Connection,
    max_timestamp: usize,
) -> Result<Vec<(String, u64, u64)>> {
    let mut statement = connection.prepare(
        "
            SELECT
                uf.token_id,
                SUM(uf.token_amount) AS total_unspent_token_amount,
                SUM(uf.sats) AS total_unspent_sats
            FROM
                utxo_funding uf
            LEFT JOIN
                utxo_spending us ON uf.new_utxo_hash = us.spent_utxo_hash
            WHERE
                (us.timestamp IS NULL OR us.timestamp > ?)
                AND uf.timestamp <= ?
            GROUP BY
                uf.token_id",
    )?;

    let tvl: Vec<(String, u64, u64)> = statement
        .query_and_then([max_timestamp, max_timestamp], |row| {
            let token_id: String = row.get(0)?;
            let token_amount: u64 = row.get(1)?;
            let sats: u64 = row.get(2)?;
            Ok((token_id, token_amount, sats))
        })
        .unwrap()
        .map(|row: Result<(String, u64, u64)>| row.unwrap())
        .collect();

    Ok(tvl)
}

#[allow(clippy::type_complexity)]
pub fn list_tokens_by_volume(
    connection: &Connection,
    seconds: usize,
    limit: usize,
) -> Result<Vec<(String, u64, u64, u64, u64, u64, u64)>> {
    // List token ID's by volume last n seconds
    let mut statement = connection
        .prepare(
            "
            WITH TradeData AS (
                SELECT
                    uf1.token_id,
                    ABS(uf1.sats - COALESCE(uf2.sats, 0)) as trade_volume
                FROM utxo_funding uf1
                LEFT JOIN utxo_funding uf2 ON uf1.spent_utxo_hash = uf2.new_utxo_hash
                WHERE uf1.timestamp >= (strftime('%s', 'now') - ?)
            ),
            TVLData AS (
                SELECT
                    token_id,
                    SUM(sats) as tvl_sats,
                    SUM(token_amount) as tvl_tokens
                FROM utxo_funding
                WHERE new_utxo_hash NOT IN (SELECT spent_utxo_hash FROM utxo_spending)
                GROUP BY token_id
            ),
            HighestUnspentUTXO AS (
                SELECT
                    token_id,
                    MAX(sats) as highest_sats,
                    token_amount
                FROM utxo_funding
                WHERE new_utxo_hash NOT IN (SELECT spent_utxo_hash FROM utxo_spending)
                GROUP BY token_id
            ),
            AggregateTradeData AS (
                SELECT
                    COALESCE(td.token_id, tvl.token_id) as token_id,
                    COALESCE(SUM(td.trade_volume), 0) as total_trade_volume,
                    COALESCE(COUNT(td.token_id), 0) as number_of_trades,
                    tvl.tvl_sats,
                    tvl.tvl_tokens,
                    hu.highest_sats,
                    hu.token_amount
                FROM TVLData tvl
                LEFT JOIN TradeData td ON tvl.token_id = td.token_id
                LEFT JOIN HighestUnspentUTXO hu ON tvl.token_id = hu.token_id
                GROUP BY tvl.token_id
            )
            SELECT
                token_id,
                total_trade_volume,
                number_of_trades,
                tvl_sats,
                tvl_tokens,
                highest_sats,
                token_amount
            FROM AggregateTradeData
            ORDER BY total_trade_volume DESC, tvl_sats DESC
            LIMIT ?
        ",
        )
        .unwrap();

    let tokens: Vec<(String, u64, u64, u64, u64, u64, u64)> = statement
        .query_and_then([seconds, limit], |row| {
            let token_id: String = row.get(0)?;
            let trade_volume: u64 = row.get(1)?;
            let trade_count: u64 = row.get(2)?;
            let tvl_sats: u64 = row.get(3)?;
            let tvl_token: u64 = row.get(4)?;
            let best_contract_sats: u64 = row.get(5)?;
            let best_contracts_token: u64 = row.get(6)?;
            Ok((
                token_id,
                trade_volume,
                trade_count,
                tvl_sats,
                tvl_token,
                best_contract_sats,
                best_contracts_token,
            ))
        })
        .unwrap()
        .map(|row: Result<(String, u64, u64, u64, u64, u64, u64)>| row.unwrap())
        .collect();

    Ok(tokens)
}
