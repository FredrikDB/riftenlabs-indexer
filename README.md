# Riftenlabs Indexer

RiftenLabs indexer is for indexing DeFi contracts by Riften Labs. It allows
for querying historical on-chain data on Bitcoin Cash.

## Contributing

Contributions are generally welcome. If you intend to make larger changes please
discuss them in an issue before MRing them to avoid duplicate work and
architectural mismatches.

## Installing Rust

Rust can be installed using your package manager of choice or
[rustup.rs](https://rustup.rs). The former way is considered more secure since
it typically doesn't involve trust in the CA system. But you should be aware
that the version of Rust shipped by your distribution might be out of date.

## Building

The library can be built and tested using [`cargo`](https://github.com/rust-lang/cargo/):

```
git clone https://gitlab.com/riftenlabs/riftenlabs-indexer.git
cd riftenlabs-indexer
cargo build
```

You can run tests with:

```
cargo test
```
